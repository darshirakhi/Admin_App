package com.example.rakhi.admin;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    EditText name,location,testtype,money,timing,testdetails;
    Button save;
    private DatabaseReference mdatabase;
    Map<String,String> arrayList=new HashMap<>();
    CoordinatorLayout coordinatorLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name=(EditText) findViewById(R.id.name);
        location=(EditText) findViewById(R.id.location);
        testtype=(EditText) findViewById(R.id.testtype);
        money=(EditText) findViewById(R.id.money);
        timing=(EditText) findViewById(R.id.timing);
        save=(Button)findViewById(R.id.save);
        mdatabase= FirebaseDatabase.getInstance().getReference();
        testdetails=(EditText)findViewById(R.id.testdetails);
        coordinatorLayout=(CoordinatorLayout)findViewById(R.id.coordinatorLayout);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name=String.valueOf(name.getText());
                String Location=String.valueOf(location.getText());
                String TestType=String.valueOf(testtype.getText());
                String Money=String.valueOf(money.getText());
                String Timing=String.valueOf(timing.getText());
                String TestDetails=String.valueOf(testdetails.getText());
                arrayList.put("Name",Name);
                arrayList.put("Location",Location);
                arrayList.put("TestType",TestType);
                arrayList.put("Money",Money);
                arrayList.put("Timing",Timing);
                arrayList.put("TestDetails",TestDetails);

                mdatabase.child("diagonostic details").push().setValue(arrayList, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        if (databaseError != null) {
                            System.out.println("message could not be saved " + databaseError.getMessage());
                        } else {
                            name.setText("");
                            location.setText("");
                            testtype.setText("");
                            money.setText("");
                            timing.setText("");
                            testdetails.setText("");
                            Snackbar.make(coordinatorLayout,"saved data successfully",Snackbar.LENGTH_LONG).show();
                            System.out.println("message saved successfully.");
                        }
                    }
                });

            }
        });

    }
}
